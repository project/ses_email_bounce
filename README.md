# AWS SES Email Bounce

Drupal is integrated with SES for sending emails. Now, our applications send
different types of emails like thank you emails, admin emails etc. Now these
admin emails are static email ids that are added across the webforms to receive
notifications. If any of these email ids have been removed from the respective
organisations then the SES receives a bounce and on every such instances the
bounce rate increases. The risk of a high bounce rate is that SES would
temporarily stop the applications email sending capability and restarting it is
a challenge.The bounce rate limit is somewhere around 4%.


## Table of contents

- Requirements
- Installation
- Configuration
- AWS SES Bounce Configuration


## Requirements

- Require Webfrom module


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Visit admin/ses_email_bounce/config to configure.
2. Enter headers which provided from AWS SES .
3. Check checkbox to save bounce email and related node and webform handler from
   where a email is removed.
4. Check email type of node fields in which you want to remove emails.
5. Check webform handlers to exclude for removing email -to_mail.
6. Bounce email log history will show in admin/ses_email_bounce/logs.


## AWS SES Bounce Configuration

1. API end point [domainname]/ses-api/subscribtion
   ( will use in AWS SES Bounce).
2. API subscribtion confirmation it will one time.
3. After confirgured api end point and called a subsctibtion will show in ses
   module dashboard at /admin/ses_email_bounce/confirmation copy that action
   url and run in your any browser and then check in ses in aws ses end point
   subscribtion is confirm or not.
