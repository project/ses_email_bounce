<?php

namespace Drupal\ses_email_bounce\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define a form that configures forms module settings.
 */
class AwsSesConfigurationForm extends ConfigFormBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type manager webform storage object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $webformStorage;

  /**
   * Constructs an AutoParagraphForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entityFieldManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->webformStorage = $this->entityTypeManager->getStorage('webform');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('entity_type.manager'),
    $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ses_email_bounce_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ses_email_bounce.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ses_email_bounce.settings');

    $form['table_info'] = [
      '#markup' => '<h3>' . $this->t('SES Configuration') . '</h3><br>',
    ];

    $form['configuration'] = [
      '#type' => 'vertical_tabs',
    ];
    $form['basic_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Basic  Settings'),
      '#group' => 'configuration',
    ];
    $form['basic_settings']['ses_api_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check for removing email and store a email bounce History log'),
      '#default_value' => $config->get('ses_api_status'),
    ];
    $form['basic_settings']['base_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base Domain'),
      '#default_value' => $config->get('base_domain'),
      '#description' => $this->t('base domain without https exp: www.[domainname].com'),
    ];

    $form['ses_header_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('SES Header Settings'),
      '#group' => 'configuration',
    ];
    $form['ses_header_tab']['header_content_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ses Header Content type'),
      '#default_value' => $config->get('header_content_type'),
      '#disabled' => TRUE,
      '#description' => $this->t('AWS SES request header content type.'),
    ];
    $form['ses_header_tab']['header_x_amz_sns_topic_arn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ses X amz sns topic arn'),
      '#default_value' => $config->get('header_x_amz_sns_topic_arn'),
      '#description' => $this->t('Ses X amz sns topic arn will be provided by AWS team'),
    ];
    $form['node_email_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Associted Node Email Fields'),
      '#group' => 'configuration',
    ];
    $existingContentTypeOptions = $this->getExistingContentTypes();
    $selected_ses_node_map = [];

    if (!empty($config->get('ses_node_map'))) {
      $selected_ses_node_map = explode(",", $config->get('ses_node_map'));
    }
    $form['node_email_fields']['ses_node_map'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Check Fields to remove associated email'),
      '#options' => $existingContentTypeOptions,
      '#default_value' => $selected_ses_node_map,
      '#description' => $this->t('Email will be removed at the time of bounce email from aws ses.'),
    ];

    $form['webform_handlers'] = [
      '#type' => 'details',
      '#title' => $this->t('Exclude Webform Handlers'),
      '#group' => 'configuration',
    ];
    $web_form_handlers = $this->getWebformHandlers();
    $selected_ses_webform_handler_exclude = [];
    if (!empty($config->get('ses_webform_handler_exclude'))) {
      $selected_ses_webform_handler_exclude = explode(",", $config->get('ses_webform_handler_exclude'));
    }

    $form['webform_handlers']['ses_webform_handler_exclude'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Exclude Webform Handlers to remove associated to email'),
      '#options' => $web_form_handlers,
      '#default_value' => $selected_ses_webform_handler_exclude,
      '#description' => $this->t('Exclude Webform Handlers to remove associated to email'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $node_fields = [];
    foreach ($form_state->getValue('ses_node_map') as $val) {
      if ($val != 0) {
        $node_fields[] = $val;
      }
    }
    $node_field_string = '';
    if (count($node_fields) > 0) {
      $node_field_string = implode(",", $node_fields);
    }

    $hander_array = [];
    foreach ($form_state->getValue('ses_webform_handler_exclude') as $val) {
      if ($val != 0) {
        $hander_array[] = $val;
      }
    }
    $handler_string = '';
    if (count($hander_array) > 0) {
      $handler_string = implode(",", $hander_array);
    }
    $this->config('ses_email_bounce.settings')
      ->set('ses_api_status', $form_state->getValue('ses_api_status'))
      ->set('header_content_type', $form_state->getValue('header_content_type'))
      ->set('header_x_amz_sns_topic_arn', $form_state->getValue('header_x_amz_sns_topic_arn'))
      ->set('base_domain', $form_state->getValue('base_domain'))
      ->set('ses_node_map', $node_field_string)
      ->set('ses_webform_handler_exclude', $handler_string)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Returns a list of all the content types's fields currently installed.
   *
   * @return array
   *   An array of content types.
   */
  public function getExistingContentTypes() {
    $types = [];
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($content_types as $content_type) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions('node', $content_type->id());
      foreach ($field_definitions as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle())) {
          if ($field_definition->getType() == 'email') {
            $types[$field_definition->getLabel() . " [" . $content_type->id() . "]"] = $field_name;
          }
        }
      }
    }
    return array_flip($types);
  }

  /**
   * Gets an array of webform handlers.
   *
   * @return array
   *   The array of webform handlers.
   */
  public function getWebformHandlers() {
    $handlers_array = [];
    $form_list = $this->webformStorage->loadMultiple();

    foreach ($form_list as $form_object) {
      if ($this->isValidWebform($form_object)) {
        $handlers_array += $this->getValidHandlers($form_object);
      }
    }

    return array_flip($handlers_array);
  }

  /**
   * Checks if a webform is valid.
   *
   * @param \Drupal\webform\WebformInterface $form_object
   *   The webform object.
   *
   * @return bool
   *   TRUE if the webform is valid, FALSE otherwise.
   */
  private function isValidWebform(WebformInterface $form_object) {
    return !$form_object->isTemplate();
  }

  /**
   * Gets an array of valid handlers for a webform.
   *
   * @param \Drupal\webform\WebformInterface $form_object
   *   The webform object.
   *
   * @return array
   *   The array of valid handlers for the webform.
   */
  private function getValidHandlers(WebformInterface $form_object) {
    $handlers_array = [];
    $webform_plugins = $form_object->getPluginCollections();
    $instances = $webform_plugins['handlers']->getInstanceIds();
    foreach ($instances as $handler_id) {
      $handler = $form_object->getHandler($handler_id);
      if ($handler instanceof EmailWebformHandler) {
        $key = $form_object->get('title') . " [" . $handler->getLabel() . "]";
        $handlers_array[$key] = $handler_id;
      }
    }

    return $handlers_array;
  }

}
