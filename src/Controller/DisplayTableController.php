<?php

namespace Drupal\ses_email_bounce\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\ses_email_bounce\Services\SesBounceService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for SES Email Bounce functionality.
 */
class DisplayTableController extends ControllerBase {

  /**
   * The SES Bounce Service.
   *
   * @var \Drupal\ses_email_bounce\Services\SesBounceService
   */
  protected $sesBounceService;

  /**
   * Constructs a DisplayTableController object.
   *
   * @param \Drupal\ses_email_bounce\Services\SesBounceService $sesBounceService
   *   The SES Bounce Service.
   */
  public function __construct(SesBounceService $sesBounceService) {
    $this->sesBounceService = $sesBounceService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ses_bounce.service')
    );
  }

  /**
   * Displays SES email bounce logs in a table.
   *
   * @return array
   *   A render array containing the log table.
   */
  public function getLogsTable() {
    $header_table = [
      'id' => $this->t('#ID'),
      'ses_timestamp' => $this->t('Date'),
      'bounced_recipients' => $this->t('Bounce Email Id'),
      'bounce_sub_type' => $this->t('Bounce Sub Type'),
      'log_detail' => $this->t('Log Detail'),
    ];
    $fields = ['id', 'type', 'subscribe_url', 'ses_timestamp'];
    $condition = ['type' => 'Notification'];
    $logs = $this->sesBounceService->searchQuery(
      'ses_email_bounce',
      $fields,
      $condition,
      NULL,
      6,
      'Drupal\Core\Database\Query\PagerSelectExtender',
      'id',
      'DESC'
    );
    $rows = [];
    if (!empty($logs)) {
      foreach ($logs as $data) {
        $id = $this->t('<a href="/admin/ses_email_bounce/logs/@id"> @id </a>', ['@id' => $data->id]);
        $detail_link = $this->t('<a href="/admin/ses_email_bounce/logs/@id">Log Detail</a>', ['@id' => $data->id]);
        $rows[] = [
          'id' => $id,
          'ses_timestamp' => $data->ses_timestamp,
          'bounced_recipients' => $data->bounced_recipients,
          'bounce_sub_type' => $data->bounce_sub_type,
          'log_detail' => $detail_link,
        ];
      }
    }
    $output['table_info'] = [
      '#markup' => '<h3>' . $this->t('SES Email Bounce List') . '</h3><br>',
    ];
    $output['ses_log_table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => $this->t('No Log found'),
    ];
    $output[] = ['#type' => 'pager'];
    return $output;
  }

  /**
   * Displays details of a specific SES email bounce log.
   *
   * @param int $id
   *   The ID of the bounce log.
   *
   * @return array
   *   A render array containing the log details table.
   */
  public function logDetail($id) {
    $result = $this->sesBounceService->searchQuery(
      'ses_email_bounce',
      [],
      ['id' => $id],
      NULL,
      NULL,
      NULL,
      NULL,
      'ASC',
      ['start' => 0 , 'end' => 1]
    );
    $result = !empty($result) ? reset($result) : FALSE;
    if ($result != FALSE) {
      $result->email_remove_from_node_ids = Json::decode($result->email_remove_from_node_ids);
    }
    $rows = [];
    if (!empty($result)) {
      $node_rows = "";
      // Check if email_remove_from_node_ids is not empty.
      if (count($result->email_remove_from_node_ids) > 0) {
        // Loop through nodes and process email_remove_from_node_ids.
        foreach ($result->email_remove_from_node_ids as $node_id => $val) {
          foreach ($val as $field_name => $v) {
            foreach ($v as $v1) {
              foreach ($v1 as $v2) {
                // Check if the value matches the bounced recipients.
                if ($v2['value'] == $result->bounced_recipients) {
                  // Build the node rows string.
                  $node_rows .= "Node:" . $node_id . "=>" . "Field:" . $field_name . "=>" . $v2['value'];
                }
              }
            }
          }
        }
      }
      $rows = [
        [
          ['data' => $this->t('Bounced Email Id'), 'header' => TRUE],
          $result->bounced_recipients,
        ],
        [
          ['data' => $this->t('SES Bounce Timestamp'), 'header' => TRUE],
          $result->ses_timestamp,
        ],
        [
          ['data' => $this->t('Bounce Type'), 'header' => TRUE],
          $result->bounce_type,
        ],
        [
          ['data' => $this->t('Bounce Sub Type'), 'header' => TRUE],
          $result->bounce_sub_type,
        ],
        [
          [
            'data' => $this->t('Email Removed From Node Fields'),
            'header' => TRUE,
          ],
          $node_rows,
        ],
        [
          [
            'data' => $this->t('Email Removed From webform Handlers'),
            'header' => TRUE,
          ],
          $result->email_remove_from_webform_handlers,
        ],
        [
          [
            'data' => $this->t('AWS SES Bounce Message Body (json)'),
            'header' => TRUE,
          ],
          $result->ses_message_body,
        ],
      ];
    }

    $output['table_info'] = [
      '#markup' => '<h3>AWS Ses Email Bounce Detail</h3><br>',
    ];

    $output['ses_log_detail_table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#empty' => $this->t('No Details found'),
    ];
    return $output;
  }

  /**
   * Displays subscriptions from SES email bounce logs.
   *
   * @return array
   *   A render array containing the subscription table.
   */
  public function getSubscriptions(): array {
    $fields = ['id', 'type', 'subscribe_url', 'ses_timestamp'];
    $subscriptions = $this->sesBounceService->searchQuery(
      'ses_email_bounce',
      $fields,
      ['type' => 'SubscriptionConfirmation']
    );
    $output['table_info'] = [
      '#markup' => '<h3>' . $this->t('API End Point Subscription Confirmation Action') . '</h3><br>',
    ];
    $rows = [];
    if (!empty($subscriptions)) {
      foreach ($subscriptions as $data) {
        $rows[] = [
          'ses_timestamp' => $data->ses_timestamp,
          'subscribe_url' => $data->subscribe_url,
        ];
      }
    }
    $header_table = [
      'ses_timestamp' => $this->t('Date'),
      'subscribe_url' => $this->t('Subscription Confirmation Action'),
    ];
    $output['ses_log_table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => $this->t('No Log found'),
    ];
    return $output;
  }

}
