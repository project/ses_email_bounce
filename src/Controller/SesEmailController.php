<?php

namespace Drupal\ses_email_bounce\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\ses_email_bounce\Services\SesBounceService;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller ses email bounce class.
 */
class SesEmailController extends ControllerBase {

  /**
   * The SES Bounce Service.
   *
   * @var \Drupal\ses_email_bounce\Services\SesBounceService
   */
  protected $sesBounceService;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type manager node storage object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $nodeStorage;

  /**
   * The entity type manager webform storage object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $webformStorage;

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The factory for configuration objects.
   *
   *   Constructs a Drupal\rest\Plugin\ResourceBase object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   A entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   A connection .
   * @param \Drupal\ses_email_bounce\Services\SesBounceService $sesBounceService
   *   The SES Bounce Service.
   */
  public function __construct(
    ConfigFactory $configFactory,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection,
    SesBounceService $sesBounceService,
  ) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->webformStorage = $this->entityTypeManager->getStorage('webform');
    $this->connection = $connection;
    $this->sesBounceService = $sesBounceService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('ses_bounce.service')
    );
  }

  /**
   * Callback for `ses-api/get` API method.
   */
  public function sesSubscribtion(Request $request) {
    $validation_message_log = [];
    $config = $this->configFactory->get('ses_email_bounce.settings');
    $ses_api_status = $config->get('ses_api_status');
    if ($ses_api_status == 0) {
      $validation_message_log['configuration'][] = 'Check for removing email and store a email bounce History log';
    }
    $header_content_type = $config->get('header_content_type');
    $header_x_amz_sns_topic_arn = $config->get('header_x_amz_sns_topic_arn');
    $base_domain_conf = $config->get('base_domain');
    $ses_node_map = $config->get('ses_node_map');
    $email_field_list = [];
    if ($ses_node_map != '') {
      $email_field_list = explode(",", $ses_node_map);
    }
    $exclude_webform_handlers = [];
    $ses_webform_handler_exclude = $config->get('ses_webform_handler_exclude');
    if ($ses_webform_handler_exclude != '') {
      $exclude_webform_handlers = explode(",", $ses_webform_handler_exclude);
    }
    $base_domain = $request->server->get('HTTP_HOST', NULL);
    if ($base_domain != $base_domain_conf) {
      if ($ses_api_status == 0) {
        $validation_message_log['configuration'][] = 'Please configure your base domain name.';
      }
    }
    $data = [];
    if (strpos($request->headers->get('Content-Type'), $header_content_type) === 0) {
      if ($request->headers->get('x-amz-sns-topic-arn') == $header_x_amz_sns_topic_arn) {
        if ($request->headers->get('x-amz-sns-message-type') == 'SubscriptionConfirmation') {
          $flag = TRUE;
          $data = json_decode($request->getContent(), TRUE);
          $subscribeURL = $data['SubscribeURL'];
          $response = new JsonResponse(['success' => $flag], 200);
          $fields = [
            'type' => 'SubscriptionConfirmation',
            'subscribe_url' => $subscribeURL,
            'ses_timestamp' => $data['Timestamp'],
            'ses_message_body' => json_encode($data),
          ];
          $this->sesBounceService->insertDataInField('ses_email_bounce', $fields);
          return $response;
        }
        elseif ($request->headers->get('x-amz-sns-message-type') == 'Notification') {
          $reqbody = preg_replace('/\s\s+/', '', $request->getContent());
          $reqbody = json_decode($reqbody, TRUE, JSON_UNESCAPED_SLASHES);
          $reqbody = json_decode($reqbody['Message'], TRUE, JSON_UNESCAPED_SLASHES);
          $data['Message'] = $reqbody;
          if (isset($data['Message']['notificationType']) && $data['Message']['notificationType'] == 'Bounce') {
            if (isset($data['Message']['bounce'])) {
              if ($data['Message']['bounce']['bounceType'] == 'Permanent') {
                if (isset($data['Message']['bounce']['bouncedRecipients'])) {
                  if ($data['Message']['bounce']['bouncedRecipients'][0]['action'] == 'failed') {
                    $ses_bounce_email = $data['Message']['bounce']['bouncedRecipients'][0]['emailAddress'];
                    $node_list = $this->prepareNodeList($ses_bounce_email, $email_field_list);
                    $node_chunk = array_chunk($node_list, 10);
                    $nodesData = [];
                    foreach ($node_chunk as $node_chunk_value) {
                      $nodesResponse = $this->processNodes($node_chunk_value, $ses_bounce_email, $email_field_list);
                      if ($nodesResponse) {
                        $nodesData = $nodesResponse;
                      }
                    }
                    // Webform list and process.
                    $form_list = $this->webformStorage->loadMultiple();
                    $webform_chunk = array_chunk($form_list, 1);
                    $webForm = [];
                    foreach ($webform_chunk as $webform_chunk_value) {
                      $webresponse = $this->processWebforms($webform_chunk_value, $ses_bounce_email, $exclude_webform_handlers);
                      if (count($webresponse) > 0) {
                        $webForm[] = $webresponse;
                      }
                    }
                    $fields = [
                      'type' => 'Notification',
                      'bounce_type' => $data['Message']['bounce']['bounceType'],
                      'bounce_sub_type' => $data['Message']['bounce']['bounceSubType'],
                      'bounced_recipients' => $ses_bounce_email,
                      'ses_message_body' => json_encode($data['Message']),
                      'email_remove_from_node_ids' => json_encode($nodesData),
                      'email_remove_from_webform_handlers' => json_encode($webForm),
                      'ses_timestamp' => $data['Message']['bounce']['timestamp'],
                    ];
                    $this->sesBounceService->insertDataInField('ses_email_bounce', $fields);
                    $response = new JsonResponse(
                      [
                        'success' => TRUE,
                        'email' => $ses_bounce_email,
                        'nodesData' => $nodesData,
                        'webForm' => $webForm,
                      ],
                      200
                    );
                    return $response;
                  }
                }
              }
            }
          }
        }
      }
      else {
        $data = json_decode($request->getContent(), TRUE);
        $validation_message_log['configuration'][] = 'Please configure Ses X amz sns topic arn.';
        $fields = [
          'type' => 'Validation',
          'subscribe_url' => '',
          'ses_timestamp' => $data['Timestamp'],
          'ses_message_body' => json_encode($validation_message_log),
        ];
        $this->sesBounceService->insertDataInField('ses_email_bounce', $fields);
      }
    }
    return new JsonResponse($validation_message_log);
  }

  /**
   * Prepare Node List.
   */
  public function prepareNodeList($email, $email_field_list) {
    $return_node_list = [];
    // Search for email in each field database table.
    foreach ($email_field_list as $field_name) {
      $query_result = $this->sesBounceService->searchQuery(
        'node__' . $field_name,
        ['entity_id', $field_name . '_value'],
        [$field_name . '_value' => "%" . $email . "%"],
        [$field_name . '_value' => 'LIKE'],
      );
      if (!empty($query_result)) {
        foreach ($query_result as $result) {
          $return_node_list[] = $result->entity_id;
        }
      }
    }
    return $return_node_list;
  }

  /**
   * Helper function to process and resave node list.
   */
  public function processNodes($options, $email, $email_field_list) {
    $node_storage = $this->nodeStorage;
    $tempNodes = [];
    foreach ($options as $nid) {
      $node_object = $node_storage->load($nid);
      foreach ($email_field_list as $email_field) {
        if ($node_object->hasField($email_field)) {
          $field_name = $email_field;
          if (!empty($field_name)) {
            $mails = $node_object->get($field_name)->getValue();
            // Hold emails before remove.
            $tempEmails = $mails;
            foreach ($mails as $key => $mail) {
              // Check for case insensitive mail ID and unset from field value.
              if (stripos($mail['value'], $email) !== FALSE) {
                unset($mails[$key]);
              }
            }
            $node_object->set($field_name, $mails);
            if (count($tempEmails) > 0) {
              $tempNodes[$nid][$field_name][] = $tempEmails;
            }
            $node_object->save();
          }
        }
      }
    }
    return $tempNodes;
  }

  /**
   * Helper function to process and resave webform handler configurations.
   */
  public function processWebforms($options, $email, $exclude_webform_handlers) {
    $entity_list_string = [];
    $form_object = $options[0];
    $webform_plugins = $form_object->getPluginCollections();
    $instances = $webform_plugins['handlers']->getInstanceIds();
    foreach ($instances as $handler_id) {
      // Filter handlers and remove handlers of custom implementations.
      if (!in_array($handler_id, $exclude_webform_handlers)) {
        $handler = $form_object->getHandler($handler_id);
        if ($handler instanceof EmailWebformHandler) {
          $handler_config = $handler->getConfiguration();
          // Update form only if the selected email id exist case insensitive.
          $email_to_type = ['to_mail', 'cc_mail', 'bcc_mail'];
          foreach ($email_to_type as $email_type) {
            if (stripos($handler_config['settings'][$email_type], $email) !== FALSE) {
              $handler_mail_list = explode(',', $handler_config['settings'][$email_type]);
              $temp_handler_mail_list = $handler_mail_list;
              foreach ($handler_mail_list as $key => $mail) {
                if (stripos($mail, $email) !== FALSE) {
                  unset($handler_mail_list[$key]);
                  $handler_mail_list_new = implode(',', $handler_mail_list);
                  $handler_config['settings'][$email_type] = $handler_mail_list_new;
                }
              }
              $handler->setConfiguration($handler_config);
              $form_object->updateWebformHandler($handler);
              if (!empty($form_object) && isset($form_object)) {
                if ($form_object->id() != '') {
                  $entity_list_string[] = [
                    'webform_id' => $form_object->id(),
                    'Handler_id' => $handler_id,
                    $email_type => $temp_handler_mail_list,
                  ];
                }
              }
            }
          }
        }
      }
    }
    return $entity_list_string;
  }

}
