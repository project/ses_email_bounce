<?php

namespace Drupal\ses_email_bounce\Services;

use Drupal\Core\Database\Connection;

/**
 * Service class for handling SES Email Bounce functionality.
 */
class SesBounceService {

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a SesBounceService object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection service.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * For inserting the data in the field.
   *
   * @param string $field_name
   *   The name of the field.
   * @param array $data
   *   The data to be entered in the field.
   */
  public function insertDataInField(string $field_name, array $data) :void {
    $this->connection->insert($field_name)->fields($data)->execute();
  }

  /**
   * Executes a search query.
   *
   * @param string $table_name
   *   The name of the table.
   * @param array $fields
   *   An array of fields to select.
   * @param array|null $conditions
   *   An associative array of conditions where the keys represent conditional
   *   fields and the values are the conditions to apply.
   * @param array|null $operator
   *   (optional) An associative array of operators where the keys correspond to
   *   fields in the conditions array and the values are the operators to apply.
   * @param int|null $limit
   *   (optional) The limit for the number of records to retrieve.
   * @param string|null $extend
   *   (optional) The extension for paginating the query results.
   * @param string|null $order_by
   *   (optional) The field to order the results by.
   * @param string $direction
   *   (optional) The direction of sorting, either 'ASC' or 'DESC'.
   * @param array|null $range
   *   (optional) The range of the result set.
   *
   * @return array
   *   An array containing the result of the search query.
   */
  public function searchQuery(
    string $table_name,
    array $fields,
    ?array $conditions = NULL,
    ?array $operator = NULL,
    ?int $limit = NULL,
    ?string $extend = NULL,
    ?string $order_by = NULL,
    string $direction = 'ASC',
    ?array $range = NULL,
  ): array {
    $query = $this->connection->select($table_name, 'data')
      ->fields('data', $fields);

    if (!is_null($conditions)) {
      foreach ($conditions as $field => $condition_data) {
        // Check if the operator exists, otherwise default to '='.
        $op = isset($operator[$field]) ? $operator[$field] : '=';
        $query->condition($field, $condition_data, $op);
      }
    }

    if (!is_null($order_by)) {
      $query->orderBy($order_by, $direction);
    }

    if (!is_null($extend)) {
      $query->extend($extend);
    }
    if (!is_null($limit)) {
      $query->range(0, $limit);
    }

    if (!is_null($range)) {
      $query->range($range['start'], $range['end']);
    }

    $query_result = $query->execute()->fetchAll();
    return $query_result;
  }

}
